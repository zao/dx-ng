use JSON;
use Data::UUID;

sub flatten {
	my $aoa = shift;
	my @ret = ();
	for $elem (@$aoa) {
		if (ref $elem eq "ARRAY") {
			push @ret, flatten($elem);
		}
		else {
			push @ret, $elem;
		}
	}
	@ret;
}

$json = JSON->new->canonical;

{
	local $/;
	$datafile = $json->decode(<STDIN>);
}

for $target (@ARGV) {
	$uc_target = uc $target;
	die "\"$target\" entry missing from input." unless $header_section = $datafile->{$target};
	$interfaces_section = $header_section->{"interfaces"};
	$enumerations_section = $header_section->{"enumerations"};
	$typedefs_section = $header_section->{"typedefs"};
	$structures_section = $header_section->{"structures"};

### Sort the interfaces base-first
	@unsorted_interfaces = sort (keys %$interfaces_section);
	%sorted_interfaces = ();
	@interfaces = ();

IFSORT:	while (@unsorted_interfaces) {
		my $iface = shift @unsorted_interfaces;
		my $bases = $interfaces_section->{$iface}->{"bases"};
		for $dep (@$bases) {
			next if (ref($dep) eq "ARRAY");
			if (not $sorted_interfaces{$dep}) {
				push @unsorted_interfaces, $iface;
				next IFSORT;
			}
		}

		push @interfaces, $iface;
		$sorted_interfaces{$iface} = 1;
	}

	open FH, ">", "$target.h";

### Generate prelude
	print FH <<EOF;
#include <rpc.h>
#include <rpcndr.h>

#if !defined(COM_NO_WINDOWS_H) && !defined(__WINESRC__)
#include <windows.h>
#include <ole2.h>
#endif

#ifndef __WIDL_${uc_target}_H
#define __WIDL_${uc_target}_H

/* Forward declarations */

EOF

### Generate forward declarations for each interface
	for $iface (@interfaces) {
		print FH <<EOF;
#ifndef __${iface}_FWD_DEFINED__
#define __${iface}_FWD_DEFINED__
typedef interface $iface $iface;
#endif

EOF
	}

	print FH <<EOF;
/* Headers for imported files */

#include <oaidl.h>
#include <ocidl.h>
#include <dxgi.h>
#include <d3dcommon.h>

#ifdef __cplusplus
extern "C" {
#endif
EOF


### Emit constants

	print FH <<EOF;
#ifndef _${uc_target}_CONSTANTS
#define _${uc_target}_CONSTANTS
#endif\n\n
EOF

### Emit macros

### Emit typedefs

	@typedefs = sort keys %$typedefs_section;
	for $typedef_name (@typedefs) {
		$typedef_value = $typedefs_section->{$typedef_name};
		print FH "typedef $typedef_value $typedef_name;\n";
	}
	print FH "\n";

### Generate enums

	@enums = sort keys %$enumerations_section;
	for $enum_name (@enums) {
		@enum = @{$enumerations_section->{$enum_name}};
		print FH "typedef enum $enum_name {";
		print FH "\n\t" if @enum;
		print FH join(",\n\t", @enum);
		print FH "\n} $enum_name;\n\n";
	}

### Order structs
	@unsorted_structs = sort keys %$structures_section;
	%sorted_structs = ();
	@structs = ();

STSORT:	while (@unsorted_structs) {
		my $struct = shift @unsorted_structs;
		my $needs = $structures_section->{$struct}->{"needs"};
		for $dep (@$needs) {
			if (not $sorted_structs{$dep}) {
				push @unsorted_structs, $struct;
				next STSORT;
			}
		}

		push @structs, $struct;
		$sorted_structs{$struct} = 1;
	}

### Generate structs

	for $struct_name (@structs) {
		$struct = $structures_section->{$struct_name};
		@elems = @{$struct->{"elements"}};
		print FH "typedef struct $struct_name {";
		print FH map { $_ = "\n\t$_;"; } @elems;
		print FH "\n} $struct_name;\n\n";
	}

### Generate each interface

	$uuid_gen = new Data::UUID;
	for $iface_name (@interfaces) {
		$iface = $interfaces_section->{$iface_name};
		@bases = flatten $iface->{"bases"};
		map { $_ = "public $_"; } @bases;

		$guid_string = $iface->{"uuid"};
		$guid = $uuid_gen->from_string($guid_string);
		@guid_parts = unpack("LSS"."C"x8, $guid);
		$guid_seq = sprintf("0x%08x, 0x%04x, 0x%04x".", 0x%02x"x8, @guid_parts);

### Emit interface prelude

		print FH <<EOF;
#ifndef __${iface_name}_INTERFACE_DEFINED__
#define __${iface_name}_INTERFACE_DEFINED__

DEFINE_GUID(IID_${iface_name}, $guid_seq); 
#if defined(__cplusplus) && !defined(CINTERFACE)
MIDL_INTERFACE("${guid_string}")
${iface_name} : @{bases}
{
EOF

### Generate methods

		@layout = @{$iface->{"layout"}};
		for $name (@layout) {
			my @method = @{$iface->{"methods"}->{$name}};
			my $ret = $method[0];
			my @params = @{$method[1]};
			map { $_ = $_->[0] if ref $_ eq "ARRAY"; } @params;
			print FH <<EOF;
	virtual ${ret} STDMETHODCALLTYPE ${name}(
		${\(join ",\n\t\t", @params)}) = 0;\n
EOF
		}

### Emit interface postlude

		print FH <<EOF;
};
#ifdef __CRT_UUID_DECL
__CRT_UUID_DECL($iface_name, $guid_seq)
#endif
#else
#endif

#endif /* __${iface_name}_INTERFACE_DEFINED__ */\n
EOF
	}

### Emit file postlude
	print FH "#endif /* __WIDL_${uc_target}_H */\n";
}
